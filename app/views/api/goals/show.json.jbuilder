json.extract! @goal, :id, :title, :description, :funds_aim, :funds_current, :funds_percentage, :payments_total_count, :payments_limit_per_page, :payments_total_pages
json.payments_current_page(@paymets.current_page)
json.payments @goal.payments.recently_added.paginate(:page => params[:page]), :id, :signature, :amount, :photo_url, :comment
