import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import 'semantic-ui-css/semantic.min.css';

const goal = document.getElementById('goal');
ReactDOM.render(<App goalId={goal.dataset.goalId} />, goal);
