import React from 'react';
import { Segment } from 'semantic-ui-react';
import img from '../../../assets/images/can.jpg';

class Can extends React.Component {
  render() {
    return (
      <div className="can-wrapper">
        <div className="can" />
        <img src={img} alt="can" />
        <div className="percentage">
          <Segment>{this.props.funds_percentage}%</Segment>
        </div>
        <h2 className="amount">
          {this.props.funds_current && this.props.funds_current.toFixed(2)} zł
        </h2>
        <h3 className="aim">CEL: {this.props.funds_aim} zł</h3>
      </div>
    );
  }
}

export default Can;
