// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>App React</div> at the bottom
// of the page.

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import axios from 'axios';

import { Container, Segment, Grid } from 'semantic-ui-react';

import Payments from './Payments';
import SupportForm from './forms/SupportForm';
import SupportFor from './SupportFor';
import Can from './Can';

class App extends React.Component {
  state = {
    goal: {}
  };

  componentWillMount() {
    axios.get(`api/goals/${this.props.goalId}`).then(response => {
      this.setState({ goal: response.data });
    });
  }

  componentDidMount() {
    setInterval(() => {
      axios.get(`api/goals/${this.props.goalId}`).then(response => {
        this.setState({ goal: response.data });
      });
    }, 5000);
  }

  render() {
    const { funds_percentage, funds_current, funds_aim } = this.state.goal;

    return (
      <Container>
        <Grid stackable>
          <Grid.Column computer={16}>
            <h1>
              {this.state.goal.title}
              <small>Skarbonka pomagacza</small>
            </h1>
          </Grid.Column>
          <Grid.Column computer={7}>
            <Segment>
              <Can
                funds_percentage={funds_percentage}
                funds_current={funds_current}
                funds_aim={funds_aim}
              />
            </Segment>
            <SupportFor />
            <Segment>
              <SupportForm goalId={this.props.goalId} />
            </Segment>
            <Segment>
              <Payments
                paymentLength={this.state.goal.payments_total_count}
                payments={this.state.goal.payments}
              />
            </Segment>
          </Grid.Column>
          <Grid.Column computer={9}>
            <Segment>
              <div
                dangerouslySetInnerHTML={{
                  __html: this.state.goal.description
                }}
              />
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
}

App.propTypes = {
  goalId: PropTypes.string.isRequired
};

export default App;
