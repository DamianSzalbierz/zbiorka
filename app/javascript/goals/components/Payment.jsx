import React from 'react';
import { Segment } from 'semantic-ui-react';
import manAvatar from '../../../assets/images/man-avatar.png';
import PropTypes from 'prop-types';

const Payment = ({ signature, comment, amount }) => (
  <Segment className="payment-wrapper">
    <img src={manAvatar} alt="avatar" width="60px" height="60px" />
    <div className="payment-content">
      <h4>{signature}</h4>
      <div>{comment}</div>
    </div>
    <span className="payment-amount">{parseFloat(amount).toFixed(2)} zł</span>
  </Segment>
);

PropTypes.defaultProps = {
  amount: 2.0
};

export default Payment;
