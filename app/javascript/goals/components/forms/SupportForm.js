import React from 'react';
import { Form, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import InlineError from '../messages/InlineError';
import axios from 'axios';

class SupportForm extends React.Component {
  state = {
    payment: {
      signature: '',
      amount: '',
      comment: '',
      photo_url: ''
    },
    loading: false,
    errors: {}
  };

  onChange = e => {
    this.setState({
      payment: { ...this.state.payment, [e.target.name]: e.target.value },
      errors: { ...this.state.errors, [e.target.name]: null }
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { payment } = this.state;
    const errors = this.validate(payment);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      axios
        .post(`/api/goals/${this.props.goalId}/createPayment`, {
          payment
        })
        .then(
          this.setState({
            payment: {
              signature: '',
              amount: '',
              comment: ''
            }
          })
        );
    }
  };

  validate = payment => {
    const errors = {};
    if (!payment.signature) errors.signature = 'Podpis nie może być pusty';
    if (!payment.amount) errors.amount = 'Kwota nie może być pusta';
    if (!payment.comment) errors.comment = 'Komentarz nie może być pusty';
    // if (!payment.photo_url) errors.photo_url = "Photo_url can't be blank";
    return errors;
  };

  render() {
    const { payment, loading, errors } = this.state;
    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        <Form.Field error={errors.signature}>
          <label htmlFor="signature">
            Podpis
            <input
              type="text"
              name="signature"
              id="signature"
              placeholder="Janek"
              value={payment.signature}
              onChange={this.onChange}
            />
          </label>
          {errors.signature && <InlineError text={errors.signature} />}
        </Form.Field>
        <Form.Field error={errors.amount}>
          <label htmlFor="amount">
            Kwota
            <input
              type="number"
              name="amount"
              id="amount"
              placeholder="2.00"
              value={payment.amount}
              onChange={this.onChange}
            />
          </label>
          {errors.amount && <InlineError text={errors.amount} />}
        </Form.Field>
        <Form.Field error={errors.comment}>
          <label htmlFor="comment">
            Komentarz
            <input
              type="text"
              name="comment"
              id="comment"
              placeholder="Komentarz"
              value={payment.comment}
              onChange={this.onChange}
            />
          </label>
          {errors.comment && <InlineError text={errors.comment} />}
        </Form.Field>
        <Form.Field error={errors.photo_url}>
          <label htmlFor="photo_url">
            Adres obrazka
            <input
              type="text"
              name="photo_url"
              id="photo_url"
              placeholder="Adres obrazka"
              value={payment.photo_url}
              onChange={this.onChange}
            />
          </label>
          {errors.photo_url && <InlineError text={errors.photo_url} />}
        </Form.Field>
        <Button primary type="submit">
          Wesprzyj
        </Button>
      </Form>
    );
  }
}

export default SupportForm;
