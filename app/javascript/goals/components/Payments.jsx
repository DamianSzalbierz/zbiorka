import React from 'react';
import Title from './section-title/SectionTitle';
import PropTypes from 'prop-types';
import Payment from './Payment';

class Payments extends React.Component {
  state = {};

  render() {
    const paymentList = this.props.payments.map(payment => (
      <Payment key={payment.id} {...payment} />
    ));
    return (
      <div>
        <Title text={`${this.props.paymentLength} osoby wsparły`} />
        {paymentList}
      </div>
    );
  }
}

Payments.propTypes = {
  payments: PropTypes.array
};

Payments.defaultProps = {
  payments: [{ id: 1, signature: 'Damian', aomunt: 2.0 }]
};

export default Payments;
