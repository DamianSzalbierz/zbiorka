import React from 'react';
import { Modal, Segment } from 'semantic-ui-react';
import celZbiorki from '../../../assets/images/celZbiorki.png';

class SupportFor extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Segment className="header-purpose" textAlign="center" attached="top">
          SKARBONKA NA
        </Segment>
        <Segment attached>
          <div className="can-purpose">
            <img src={celZbiorki} alt="image" />
            <Modal
              trigger={
                <h3>Pomóż mi zatrzymać chorobę, która zabija moje dziecko!</h3>
              }
            >
              <Modal.Header>POMOŻ!!</Modal.Header>
              <Modal.Description>
                <p>
                  Lorem Ipsum jest tekstem stosowanym jako przykładowy
                  wypełniacz w przemyśle poligraficznym. Został po raz pierwszy
                  użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem
                  próbnej książki. Pięć wieków później zaczął być używany
                  przemyśle elektronicznym, pozostając praktycznie
                  niezmienionym. Spopularyzował się w latach 60. XX w. wraz z
                  publikacją arkuszy Letrasetu, zawierających fragmenty Lorem
                  Ipsum, a ostatnio z zawierającym różne wersje Lorem Ipsum
                  oprogramowaniem przeznaczonym do realizacji druków na
                  komputerach osobistych, jak Aldus PageMaker
                </p>
              </Modal.Description>
            </Modal>
          </div>
        </Segment>
      </div>
    );
  }
}

export default SupportFor;
