// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import axios from 'axios';

class Hello extends React.Component {
  state = {
    goal: {}
  };

  componentWillMount() {
    axios.get('api/goals/7').then(response => {
      this.setState({ goal: response.data });
    });
  }

  componentDidMount() {
    setInterval(() => {
      axios.get('api/goals/7').then(response => {
        this.setState({ goal: response.data });
      });
    }, 5000);
  }

  render() {
    return (
      <div>
        <h1>{this.state.goal.title}</h1>
        <div
          dangerouslySetInnerHTML={{ __html: this.state.goal.description }}
        />
      </div>
    );
  }
}

Hello.defaultProps = {
  name: 'David'
};

Hello.propTypes = {
  name: PropTypes.string
};

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Hello name="React" />,
    document.body.appendChild(document.createElement('div'))
  );
});
