class Payment < ApplicationRecord
  belongs_to :goal
  validates :signature, :amount, :comment, :goal_id, presence: true

  scope :recently_added, -> { order('created_at DESC') }
  self.per_page = 10
end
