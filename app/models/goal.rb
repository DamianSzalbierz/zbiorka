class Goal < ApplicationRecord
  has_many :payments
  validates :title, :description, :funds_aim, presence: true

  def funds_current
    self.payments.map { |payment| payment.amount.to_f }.sum.round(2)
  end

  def funds_percentage
    (self.payments.map { |payment| payment.amount.to_i }.sum * 100) / self.funds_aim
  end

  def payments_total_count
    self.payments.length
  end

  def payments_limit_per_page
    self.payments.per_page
  end

  def payments_total_pages
    (self.payments.length.to_f / 10.to_f).ceil
  end

  def payments_current_page
    self.payments.params[:page]
  end

end
