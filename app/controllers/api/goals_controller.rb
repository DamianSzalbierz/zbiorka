class Api::GoalsController < ApplicationController
  protect_from_forgery with: :null_session

  def show
    @goal = Goal.find(params[:id])
    @paymets = @goal.payments.recently_added.paginate(:page => params[:page])
  end

  def createPayment
    @payment = Payment.new(payment_params)
    @payment.goal_id = params[:id]
    if @payment.save
      render json: @payment, status: :created
    else
      render json: @payment.errors, status: :unprocessable_entity
    end
  end

  private

  def payment_params
    params.require(:payment).permit(:signature, :amount, :comment, :photo_url)
  end
end
