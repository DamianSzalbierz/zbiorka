Rails.application.routes.draw do
  root 'pages#home'
  namespace :api, defaults: { format: :json } do
    resources :goals, only: :show do
      post 'createPayment', on: :member
    end
  end
end
