class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.string :signature
      t.decimal :amount, precision: 8, scale: 2
      t.text :comment
      t.references :goal, foreign_key: true

      t.timestamps
    end
  end
end
