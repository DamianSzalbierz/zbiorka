class AddPhotoUrlToPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :photo_url, :string
  end
end
