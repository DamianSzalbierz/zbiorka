class AddFundsAimToGoals < ActiveRecord::Migration[5.1]
  def change
    add_column :goals, :funds_aim, :integer
  end
end
