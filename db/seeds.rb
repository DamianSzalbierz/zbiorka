# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Payment.delete_all
Goal.delete_all

Goal.create!([
  {
    title: "CHARYTATYWNY STREAM 72H IV EDYCJA",
    description: "<p><strong>IV EDYCJA STREAMA CHARYTATYWNEGO (15-17 grudnia 2017)<br></strong></p>\n<p><img src=\"https://siepomagaimg.pl/uploads/attached_image/photo/109439/content_1513327493.png\"></p>\n<p>Już po raz czwarty mamy przyjemność razem z Wami przeżyć jedno z najlepszych wydarzeń w roku - 72 godzinny stream charytatywny! Pierwszy raz zebraliśmy się wszyscy w jednym miejscu, by wspólnie streamować, świetnie się bawić, ale przede wszystkim - pomagać. W tym roku chcemy pomóc <strong>Hubertowi</strong>, który cierpi na zanik mięśni. Jego choroba jest przeszkodą , która uniemożliwia mu normalne życie. Na święta chcemy wraz z całym internetem podarować mu najważniejszą wartość na świecie - zdrowie. Pokażmy ile dobrego razem możemy zdziałać! </p>\n<p><a href=\"https://www.twitch.tv/fantasypomaga\" rel=\"nofollow\" target=\"_blank\">Streamujemy TUTAJ &lt;- kliknij! </a></p>\n<p><img src=\"https://siepomagaimg.pl/uploads/attached_image/photo/109402/content_1513323195.png\"></p>\n<p>Plan streama:</p>\n<p>● PIĄTEK<br>10:00-14:00 - WSZYSCY <br>14:00-16:00 - Kamyk Jcob Vibe<br>16:00-18:00 - Lachu Dev Koza N3<br>18:00-22:00 - WSZYSCY<br>22:00-02:00 - Kamyk Vibe<br>● SOBOTA<br>02:00-09:00 - Jcob Dev Koza<br>09:00-12:00 - Lachu Vibe<br>12:00-15:00 - Kamyk n3<br>15:00-18:00 - Jcob Lachu Koza<br>18:00-23:00 - WSZYSCY<br>23:00-01:00 - Dev N3jxiom <br>● NIEDZIELA<br>01:00-09:00 - Lachu Vibe<br>09:00-12:00 - Jcob Kamyk<br>12:00-15:00 - Koza N3jxiom Dev<br>15:00-18:00 - Lachu Jcob Vibe<br>18:00-22:00 - WSZYSCY<br>22:00-01:00 - Dev Koza<br>● PONIEDZIAŁEK<br>01:00-08:00 - Kamyk N3jxiom<br>08:00-10:00 - WSZYSCY</p>\n<p><iframe src=\"//www.youtube.com/embed/WGcoEIY1Pqw?color=white&amp;iv_load_policy=3&amp;rel=0&amp;showinfo=0&amp;playsinline=1\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\" data-vertical=\"false\"></iframe></p>\n<p> </p>",
    funds_aim: 1500
  }
  ])

puts "Goal seeded!"

100.times do
  Payment.create!(signature: 'Annonymous', amount: '2.21', comment: 'Anonimowa pomoc', goal_id: Goal.first.id, photo_url: 'https://www.siepomaga.pl/assets/siepomaga/avatars/sms-b8e05dd42d596ee11f2f70911bc4ee7920aba5a8cb679ca1346db8d40cbe674d.png')
end

Payment.create!(signature: 'Damian', amount: '2', comment: 'Szczytny cel', goal_id: Goal.first.id)
Payment.create!(signature: 'Bartek', amount: '4', comment: 'Oby pomoglo', goal_id: Goal.first.id)
Payment.create!(signature: 'Krzysiek', amount: '2', comment: 'Pomoc na swieta', goal_id: Goal.first.id)

puts "Payments seeded!"
